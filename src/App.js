import React, { Component } from 'react';
import Chart from './components/Chart'
import './App.css';

class App extends Component {
  render() {
    return (
      <div>
        <Chart />
      </div>
    );
  }
}

export default App;
